package com.mandeep.kafkaspring.model;

public class User {
    private int id;
    private String name;
    private float salary;

    public User(int id, String name, float salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    public User() {
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", salary=" + salary +
                '}';
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }
}
