package com.mandeep.kafkaspring.resource;

import com.mandeep.kafkaspring.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("kafka")
public class UserResource {

    @Autowired
    KafkaTemplate<String, User> kafkaTemplate;
    public static final String TOPIC = "quickstart-event";
//    public static final String TOPIC = "kafkaExp";

    @GetMapping("/publish/{name}")
    public String post(@PathVariable final String name){
        kafkaTemplate.send(TOPIC,new User(1,name,2345));
        return "Published Successfully";
    }
    @RequestMapping(value = "/save",method = RequestMethod.POST)
    public String save(@RequestBody User user){
        kafkaTemplate.send(TOPIC,user);
        return "Saved Successfully";
    }

}
