package com.mandeep.avrodemo;

import org.apache.avro.specific.SpecificDatumWriter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AvroDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AvroDemoApplication.class, args);

		SpecificDatumWriter specificDatumWriter = new SpecificDatumWriter();
	}

}
