## history:
- **day1**:
    - Setting Up Laptop and Installing Packages
- **day2**:
    - API
    - REST
    - SOAP
    - BACKEND
    - FRONTEND
- **day3**:
    - HTTP Methods
    - Clint-Server Model
    - Git
- **day4**:
    - Git
    - HTML
- **day5**:
    - HTML
    - CSS
- **day6**:
    - CSS
- **day7**
    - BootStrap
    - JavaScript
    - JQuery
    - XML
    - JSON
- **day8**
    - AJAX
    - SQL
    - MYSQL
- **day9**
    - SQL: Hackerrank
- **day10**
    - SQL: Hackerrank
    - Problem Solving: Hackerrank
- **day11**
    - Java Tutorial for Complete Beginners [Udemy.com](https://www.udemy.com/course/java-tutorial/) (Chapter: 1-42)

- **day12**
    - Java Tutorial for Complete Beginners [Udemy.com](https://www.udemy.com/course/java-tutorial/) (Chapter: 43-59)

- **day13**
    - Java Tutorial for Complete Beginners [Udemy.com](https://www.udemy.com/course/java-tutorial/) (Chapter: 60-69)
    
- **day14**
    - Java Tutorial for Complete Beginners [Udemy.com](https://www.udemy.com/course/java-tutorial/) (Chapter: 70)

- **day15**
    - Modern Java Programming, Lambda and more [Udemy.com](https://www.udemy.com/course/java-latest-programming-from-zero-java13-java12-java11-java10-java9-j8/) (Chapter: 11-15)
       
- **day16**
    - Modern Java Programming, Lambda and more [Udemy.com](https://www.udemy.com/course/java-latest-programming-from-zero-java13-java12-java11-java10-java9-j8/) (Chapter: 16-43)
       
- **day17**
    - Modern Java Programming, Lambda and more [Udemy.com](https://www.udemy.com/course/java-latest-programming-from-zero-java13-java12-java11-java10-java9-j8/) (Chapter: 44-63)
       
- **day18**
    - Modern Java Programming, Lambda and more [Udemy.com](https://www.udemy.com/course/java-latest-programming-from-zero-java13-java12-java11-java10-java9-j8/) (Chapter: 64-108)
          
- **day19**
    - Modern Java Programming, Lambda and more [Udemy.com](https://www.udemy.com/course/java-latest-programming-from-zero-java13-java12-java11-java10-java9-j8/) (Chapter: 109-119)
    - YouTube Videos for Java 14, 15, 16
        - https://www.youtube.com/watch?v=viBK0LVeBnc
        - https://www.youtube.com/watch?v=IvytsoAUEZA
        - https://www.youtube.com/watch?v=s3otQAhPNZg
- **day20**
    - Spring in Java Framework, Maven
- **day21**
    - Spring Boot Unit 1 [YouTube Video playlist about Spring Boot](https://www.youtube.com/playlist?list=PLqq-6Pq4lTTbx8p2oCgcAQGQyqN8XeA1x)
- **day22**
    - Spring Boot Unit 2 [YouTube Video playlist about Spring Boot](https://www.youtube.com/playlist?list=PLqq-6Pq4lTTbx8p2oCgcAQGQyqN8XeA1x)
- **day23**
    - Spring Boot Unit 3,4,5 [YouTube Video playlist about Spring Boot](https://www.youtube.com/playlist?list=PLqq-6Pq4lTTbx8p2oCgcAQGQyqN8XeA1x)
- **day24**
    - JavaScript Fundamentals
    - Spring Security (OAuth & JWT) [Youtube Playlist](https://www.youtube.com/playlist?list=PLqq-6Pq4lTTYTEooakHchTGglSvkZAjnE)
- **day25**
    - In Mamory Authantication For Different Paths
- **day26**
    - JavaScript from http://javascript.info/
    - JavaScript Assigment
- **day27**
    - JavaScript from http://javascript.info/
    - JavaScript Assigment
- **day28**
    - Authantication Using JDBC using Spring
- **day29**
    - Authantication Using JDBC using Spring
- **day30**
    - Implementing UserDetailsService to Authanticate
    - Authantication Using LDAP using Spring
- **day31**
    - Authantication Using JWT
    - Kafka
    - Implementation of kafka Producer in spring using KafkaTemplate
- **day32**
    - Avro
- **day33**
    - Kafka Producer and consumer with Avro
    - Kafka Producer with JSON
- **day34**
    - Confluent Kafka
- **day35**
    - Jackson
    - Sybase IQ
    - ActiveMQ
- **day36**
    - Drools
- **day37**
    - 1 Test 
    - POC for multiple MQ
- **day38**
    - Servlets
- **day39**
    - Apache Spark
    - 2 Tests
    - Servlet JDBC
- **day40**
    - Apache Spark with kafka
- **day41**
    - Installation of Kafka avtivemq connector
    - SLF4J Logger
    - 1 Test
- **day42**
    - Test (Abhijeet)
- **day43**
    - Test (Abhijeet)
    - RabbitMQ Producer Spring Boot
- **day44**
    - JUnit
- **day45**
    - AWS IAM
    - AWS S3
- **day46**
    - AWS EC2
    - AWS Lambda
    - Connecting EC2 to s3
    - Tests(2)
- **day47**
    - AWS Lambda Triggers
    - AWS CloudWatch
- **day48**
    - Support (LxM)
    - Test (1)
- **day49**
    - Support (LxM)
    - Test (2)
    - Postgres Database
- **day50**
    - Test (1)
    - Support (LxM)
- **day51**
    - Support (LxM)
    - Hadoop Spark Azure
- **day52**
    - Support (LxM)
    - Creating Docker images
- **day53**
    - Support (LxM)
    - Container Orchestration
    - Elastic Container Registry
- **day54**
    - Elastic Container Service
    - DynamoDB
- **day55**
    - JavaRDD, DataFrame, DataSet
    - Installing and setting up Spark
    - Creating Spark Jobs (No Success)
    - Spring Dependancy Injection
- **day56**
    - Traning Dhranai (Collections and Generic Classes in Java)
    - AWS Developer course at udamy (Dynamo DB) [Link](https://www.udemy.com/course/aws-certified-developer-associate-dva-c01/learn/lecture/12203046#overview)
    - Running Spark job(Success)
- **day57**
    - Traning Dhranai (Execption Handling and Multithreading)
    - Test with Abhijeet and Gyanendra
    - AWS Billing and Budget
- **day58**
    - Communication and Synchronization between threads
    - Traning Mehul (Spring Boot)
    - Spring Cloud
- **day59**
    - Functional Interfaces and Lambdas
    - Spring Cloud Functions
    - 1 Test with Raj and Nikhil
    - CI and SI and Autowiring [Javatpoint.com](https://www.javatpoint.com/spring-tutorial)
- **day60**
    - Udamy Course: [Spring 5](https://www.udemy.com/course/spring-5-with-spring-boot-2)
    - Traning Mehul (AWS)
    - Test with Gyanendra
    - Test Nikhil, Aditya, Ayush (SpringBoot)
- **day61**
    - Traning Mehul (Spring Boot JPA with MySQL)
    - Spring JdbcTemplate
- **day62**
    - Traning Mehul (JPA Repository)
    - Spring Cloud Functions
- **day63**
    - Traning Mehul
    - Udamy Course: [Spring 5](https://www.udemy.com/course/spring-5-with-spring-boot-2)
    - AWS Lambda & API Gateway
- **day64**
    - Traning Mehul
    - Lambda Concurrancy and layers on AWS
- **day65**
    - Creating Java files from Java class files
    - Creating Lambda layers for AWS Lambda




    
# Pre Placement ToDo
### GIT [8 Hours]
- [x] Ref: https://www.w3schools.com/git/default.asp
- [ ] Course: https://www.katacoda.com/courses/git
- [ ] Practice: https://learngitbranching.js.org/ 
- [x] Exercises:https://www.w3schools.com/git/exercise.asp?filename=exercise_getstarted1

### - HTML [4 Hours]
- [x] Ref: https://www.w3schools.com/html/default.asp
- [x] Exercises: https://www.w3schools.com/html/html_exercises.asp

### CSS [2 Hours]
- [x] Ref: https://www.w3schools.com/css/default.asp
- [x] Exercises: https://www.w3schools.com/css/css_exercises.asp

### Bootstrap [2 Hours]
- [x] Ref: https://www.w3schools.com/bootstrap/bootstrap_ver.asp
- [x] Exercises: https://www.w3schools.com/bootstrap4/bootstrap_exercises.asp

### JavaScript [10 Hours]
- [x] Ref: https://www.w3schools.com/js/default.asp
- [x] Exercises: https://www.w3schools.com/js/js_exercises.asp

### JQuery [6 hours]
- [x] Ref: https://www.w3schools.com/jquery/default.asp

### XML AND JSON [2 hours]
- [x] XML Ref: https://www.w3schools.com/xml/default.asp
- [x] JSON Ref: https://beginnersbook.com/2015/04/json-tutorial/

### XML AJAX [2 hours]
- [x] Ref: https://www.w3schools.com/xml/ajax_intro.asp

### SQL [8 Hours]
- [x] Ref: https://www.w3schools.com/sql/default.asp
- [x] Exercises: https://www.w3schools.com/sql/sql_exercises.asp
                           
### MYSQL [3 Hours]
- [x] Ref: https://www.w3schools.com/mysql/mysql_exercises.asp
- [x] Exercises: https://www.w3schools.com/mysql/mysql_exercises.asp

---

**Training Notes**

This file is for keeping notes of my training.
---

## Installing packages

How to install different type of packages on mac.

1. For installing JDK, download compressed file and extract it then move it using: `sudo mv jdk-16.0.1.jdk /Library/Java/JavaVirtualMachines/`
2. ...

---

## NodeJS libraries:

List of useful JS Libraries.

1. Express.js: Express is a minimal and flexible Node.js web application framework that provides a robust set of features for web and mobile applications.
    - command: `npm i express`
2. Joi: The most powerful schema description language and data validator for JavaScript.
    - command: `npm i joi`
3. Nodemon: nodemon is a tool that helps develop node.js based applications by automatically restarting the node application when file changes in the directory are detected.
    - command: `npm i nodemon`

## GIT
- **git init**: To initalize git repository 
- `git add name` : To add specific file in staging area.
- `git add -all` : Add all file to staging area.
- `git push remote_name` : Push Repository to another repository.
- `git remote add path_or_url` : Add remote repositories.
- `git commit -m 'message'` : Commiting Changes.
- `git pull repository_url` : To get updates from remote repository


## HTML

- **Emoji reference**: https://www.w3schools.com/charsets/ref_emoji.asp
- **Currency Reference**: https://www.w3schools.com/charsets/ref_utf_currency.asp
- **Arrows Reference**: https://www.w3schools.com/charsets/ref_utf_arrows.asp
- **Symbols Reference**: https://www.w3schools.com/charsets/ref_utf_symbols.asp
- **Float Examples**: https://www.w3schools.com/css/css_float_examples.asp

## CSS
- **CSS Pseudo Classes Selector**: https://www.w3schools.com/css/css_pseudo_classes.asp
- **box-sizing : border-box** This makes sure that the padding and eventually borders are included in the total width and height of the elements.

## XML
- **XPath**: XPath is a syntax for defining parts of an XML document and it uses path expressions to navigate in XML documents
- **XSLT**: (eXtensible Stylesheet Language Transformations) is the recommended style sheet language for XML.
- **XQuery**: XQuery is a language for finding and extracting elements and attributes from XML documents.
- **XLink**: XLink is used to create hyperlinks within XML documents and any element in an XML document can behave as a link.
- **XPointer**: XPointer allows links to point to specific parts of an XML document.


## SQL
- **Table Pivot**:

    - input
    columns: (Name, Occupation)

    SELECT  
    [doctor],[professor],[singer],[actor]
    FROM  
        (SELECT ROW_NUMBER() OVER (PARTITION BY OCCUPATION ORDER BY NAME) [RowNumber], * FROM OCCUPATIONS)   
    AS src  
    PIVOT  
    (  
        MAX(name) FOR occupation IN ( [doctor], [professor],[singer],[actor])  
    ) AS pvt;

    - output
    columns: (doctor, professor, singer, actor)

## SpringBoot Annotations
- `@SpringBootApplication` : Application Class
- `@RestController` : Rest Controller Class
- `@RequestMapping` : Maps only the GET method by default. To map to other HTTP Methods we need to specify it in annotation 
    Example:
    1. `@RequestMapping("/hello")`
    2. `@Request Mapping("/topics/{id}")` : id will be used as parameter
    3. `@RequestMapping(method = RequestMethod.PUT, value = "/topics/{id}")`: Specifing Methods for Mapping
- `@Service` : Service Class, Single Instance
- `@Autowired` : AutoInitalize Service Class Variables
- `@EnableWebSecurity`: To get auth object in SecurityConfiguration class which extends WebSecurityConfigurerAdapter class
- `@Component`: To create Components like Filters
- [Security Database Schema](https://docs.spring.io/spring-security/site/docs/3.0.x/reference/appendix-schema.html)



## Spring CrudRepository
- `findByName(T value)` : Find all with value in column name
- `findByClassId(T value)`: Finds all with column name class which have id equals to value

## Maven
- `mvn spring-boot:build-image` : To build image of a spring-boot project
