package com.mandeep.rabbitmqdemo.mqs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.stereotype.Service;

@Service
public class RabbitMQListener implements MessageListener {

    //uncompleted
    @Override
    public void onMessage(Message message) {
        System.out.println(message.getBody());
    }
}
