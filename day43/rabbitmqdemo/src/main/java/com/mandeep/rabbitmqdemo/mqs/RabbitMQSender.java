package com.mandeep.rabbitmqdemo.mqs;

import com.mandeep.rabbitmqdemo.model.Employee;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class RabbitMQSender {
    @Autowired
    AmqpTemplate amqpTemplate;

    @Value("${mandeep.rabbitmq.exchange}")
    String exchange;

    @Value("${mandeep.rabbitmq.routingKey}")
    String routingKey;

    public void send(Employee employee){
        for(int i=1 ;i<101;i++){
            employee.setId(i);
            try {
                amqpTemplate.convertAndSend(exchange,routingKey,employee);
                System.out.println("Sending: "+ employee);
                TimeUnit.MILLISECONDS.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
