package com.mandeep.rabbitmqdemo;

import com.mandeep.rabbitmqdemo.config.RabbitMQConfig;
import com.mandeep.rabbitmqdemo.model.Employee;
import com.mandeep.rabbitmqdemo.mqs.RabbitMQSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@SpringBootApplication
public class RabbitmqdemoApplication {

	@Autowired
	RabbitMQSender rabbitMQSender;
	public static void main(String[] args) {
		SpringApplication.run(RabbitmqdemoApplication.class, args);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/send")
	public ResponseEntity<String> pushInQueue(@RequestBody Employee employee){
		rabbitMQSender.send(employee);
		return new ResponseEntity<>("Sent Successfully", HttpStatus.OK);
	}

}
