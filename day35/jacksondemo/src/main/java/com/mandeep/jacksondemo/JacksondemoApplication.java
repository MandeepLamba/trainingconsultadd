package com.mandeep.jacksondemo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.ResourceLoader;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@SpringBootApplication
public class JacksondemoApplication {

	@Autowired
	ResourceLoader resourceLoader;

	public static void main(String[] args) {
		SpringApplication.run(JacksondemoApplication.class, args);

		ObjectMapper objectMapper = new ObjectMapper();

//		String carJson = "{ \"brand\": \"Mercedes\",\"doors\":5, \"tyres\":4}";

		try{

			File file = new ClassPathResource("lambo.json").getFile();

			//to ignore extra properties
//			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);

//			List<Car> cars = objectMapper.readValue(file,new TypeReference<>(){});
//			for (Car car:
//				 cars) {
//				System.out.println("car.brand = " + car.brand);
//				System.out.println("car.doors = " + car.doors);
//				System.out.println("car.hp = " + car.hp);
//			}
//			System.out.println(objectMapper.writeValueAsString(cars.get(0)));
//			Map<String,Object> cars = objectMapper.readValue(file,new TypeReference<>(){});
//			cars.forEach((k,v)->{
//				System.out.println("car "+k+" is " + v);
//			});

			JsonNode jsonNode = objectMapper.readValue(file,JsonNode.class);
			System.out.println("jsonNode.size() = " + jsonNode.size());
			System.out.println(jsonNode.get(0).get("brand"));
			System.out.println(jsonNode.get(1).get("name"));
			System.out.println(jsonNode.get(1).get("hp"));

		} catch (JsonProcessingException e) {
			e.printStackTrace();
//			System.out.println("Error: " + e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	record Car(String brand, Integer doors, Integer hp){}
//	class Car{
//		String brand;
//		int doors;
//		Integer tires;
//		Car(){}
//}
}
