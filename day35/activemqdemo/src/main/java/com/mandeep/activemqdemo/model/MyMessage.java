package com.mandeep.activemqdemo.model;

import java.io.Serializable;

public class MyMessage implements Serializable {

    private String src;
    private String msg;

    @Override
    public String toString() {
        return "MyMessage{" +
                "src='" + src + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
