package com.mandeep.activemqdemo.consumer;

import com.mandeep.activemqdemo.model.MyMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class MessageConsumer {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageConsumer.class);

    @JmsListener(destination = "testmq")
    public void logMessage(MyMessage message) {
        LOGGER.info("Message Received: {}", message);
    }
}
