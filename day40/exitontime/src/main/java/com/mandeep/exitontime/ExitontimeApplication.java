package com.mandeep.exitontime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
public class ExitontimeApplication {

	static Logger logger = LoggerFactory.getLogger(ExitontimeApplication.class);

	public static void main(String[] args) throws InterruptedException {
		SpringApplication.run(ExitontimeApplication.class, args);

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter Seconds: ");
		int sec = scanner.nextInt();
		long time = System.currentTimeMillis()+sec*1000;
		while(System.currentTimeMillis() < time){
			logger.trace("T Running...");
			logger.debug("D Running...");
			logger.info("I Running...");
			logger.warn("W Running...");
			logger.error("E Running...");
			TimeUnit.SECONDS.sleep(1);
		}
	}

}
