package com.example.awsec2spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Awsec2springApplication {

	static Logger logger = LoggerFactory.getLogger(Awsec2springApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(Awsec2springApplication.class, args);

	}

}
