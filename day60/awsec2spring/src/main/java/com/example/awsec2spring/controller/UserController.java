package com.example.awsec2spring.controller;

import com.example.awsec2spring.model.User;
import com.example.awsec2spring.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    UserRepo userRepo;


    @GetMapping("/")
    public String home(){
        return "<h1>Welcome</h1>";
    }

    @GetMapping("/users")
    public List<User> getUsers(){
        return userRepo.getUsers();
    }


}
