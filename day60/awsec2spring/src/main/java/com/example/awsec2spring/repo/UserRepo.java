package com.example.awsec2spring.repo;

import com.example.awsec2spring.model.User;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Repository
public class UserRepo {

    public List<User> getUsers(){
        return Arrays.asList(
                new User(12,"Abc"),
                new User(13, "Yum"),
                new User(14, "Linux")
        );
    }
}
