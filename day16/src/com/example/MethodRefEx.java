package com.example;

import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public class MethodRefEx {
    public static void main(String[] args) {
        Predicate<Teacher> online = teacher -> teacher.isTeachOnline();
        Predicate<Teacher> online1 = Teacher::isTeachOnline;

        Function<Integer,Double> sqrt = Math::sqrt;

        Function<String,String> toUp = String::toUpperCase;

        UnaryOperator<Integer> mul = MethodRefEx::x10;
        System.out.println(mul.apply(3));

    }

    public static int x10(int a){
        return a*10;
    }
}
