package com.example;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class PredicateEx {
    public static void main(String[] args) {
        //predicate if number is greater then 10
        Predicate<Integer> pr = (x) -> x > 10;
        System.out.println(pr.test(22));
        System.out.println(pr.test(3));

        //predicate for number is even
        Predicate<Integer> peven = (x) -> x%2==0;
        System.out.println(peven.test(32));
        System.out.println(peven.test(63));

        //using both (even and greater then 10)
        System.out.println(pr.and(peven).test(32));
        System.out.println(pr.and(peven).test(8));
        System.out.println(pr.or(peven).test(8));

        List<Teacher> teachers = Teachers.getAll();

        Predicate<Teacher> onlinePr = Teacher::isTeachOnline;
        Predicate<Teacher> malePr = teacher -> teacher.getGender().equals("M");

        // list of teacher who are male and teaches online also

        teachers.forEach(teacher -> {
            if(onlinePr.and(malePr).test(teacher)) System.out.println(teacher.getName());
        });

        //With BiConsumer
        BiConsumer<String,List<String>> tCourse = (name, courses) -> System.out.println(name + " teaches "+ courses);

        System.out.println("------------");
        teachers.forEach(teacher -> {
            if(onlinePr.and(malePr).test(teacher)){
                tCourse.accept(teacher.getName(),teacher.getCourses());
            }
        });

        //With BiPredicate

        BiPredicate<Boolean,String> bp = (online, gender) -> online && gender.equals("F");
        System.out.println("------------");
        teachers.forEach(teacher -> {
            if(bp.test(teacher.isTeachOnline(),teacher.getGender())){
                tCourse.accept(teacher.getName(),teacher.getCourses());
            }
        });



    }
}
