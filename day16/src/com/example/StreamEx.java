package com.example;

import java.util.function.Predicate;
import java.util.stream.Collectors;

public class StreamEx {
    public static void main(String[] args) {
        Predicate<Teacher> agePr = teacher -> teacher.getAge()>30;
        Predicate<Teacher> onlinePr = Teacher::isTeachOnline;

        System.out.println(Teachers.getAll().stream()
                 .filter(agePr)
                 .filter(onlinePr)
                 .collect(Collectors
                         .toMap(Teacher::getName,Teacher::getCourses)));

        //map()
        System.out.println(Teachers.getAll().stream()
                .map(Teacher::getName)
                .map(String::toUpperCase)
                .collect(Collectors.toList()));
    }
}
