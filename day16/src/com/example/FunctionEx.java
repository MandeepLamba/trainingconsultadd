package com.example;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.*;

public class FunctionEx {
    public static void main(String[] args) {

        Function<Integer,Double> sqrt = integer -> Math.sqrt(integer);

        System.out.println("Square Root of 64: "+ sqrt.apply(64));

        Function<String, String> concat = s -> s.concat(" Says Hello!");
        Function<String, String> toUp = s -> s.toUpperCase();

        System.out.println(toUp.compose(concat).apply("Mandeep"));


        //Function to create map from list of teachers

        Function<List<Teacher>, Map<String,String>> mapFunction = teachers -> {
            Map<String, String> map = new HashMap<>();
            teachers.forEach(teacher -> map.put(teacher.getName(),teacher.getGender().equals("M")?"Male":"Female"));
            return map;
        };

        System.out.println(mapFunction.apply(Teachers.getAll()));

        //if wanna choose teacher who teaches 3 courses
        Predicate<Teacher> count = teacher -> teacher.getCourses().size()>2;
        Function<List<Teacher>, Map<String,String>> mapFunction1 = teachers -> {
            Map<String, String> map = new HashMap<>();
            teachers.forEach(teacher -> {
                if(count.test(teacher)) map.put(teacher.getName(),teacher.getGender().equals("M")?"Male":"Female");
            });
            return map;
        };
        System.out.println(mapFunction1.apply(Teachers.getAll()));


        //BiFunctions
        BiFunction<List<Teacher>,Predicate<Teacher>,Map<String,Integer>> biFunction = (teachers, teacherPredicate) -> {
            Map<String,Integer> map = new HashMap<>();
            teachers.forEach(teacher -> {
                if(teacherPredicate.test(teacher)) map.put(teacher.getName(),teacher.getCourses().size());
            });
            return map;
        };

        System.out.println(biFunction.apply(Teachers.getAll(),count));

        //unaryOperator
        UnaryOperator<Integer> x10 = x -> x*10;
        System.out.println(x10.apply(4 ));

        //BinaryOperator
        BinaryOperator<Long> mul = ((aLong, aLong2) -> aLong*aLong2);
        System.out.println(mul.apply(345l,322l));

        //Comparator
        Comparator<Integer> comparator = (a,b) -> a.compareTo(b);
        BinaryOperator<Integer> max = BinaryOperator.maxBy(comparator);
        BinaryOperator<Integer> min = BinaryOperator.minBy(comparator);
        System.out.println(max.apply(44,65));
        System.out.println(min.apply(44,65));



    }
}
