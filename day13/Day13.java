import java.util.HashMap;

public class Day13 {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();
        phonebook.addContact("Mandeep", 9993344334l);
        phonebook.addContact("Param", 334343334l);

        System.out.println(phonebook.getContect("Mandeep"));
    }
}
class Phonebook{
    private HashMap<Character,HashMap<String,Long>> phone = new HashMap<>();

    public void addContact(String name, Long number){
        Character firstChar = Character.toUpperCase(name.charAt(0)); 
        if(!phone.containsKey(firstChar))
            phone.put(firstChar, new HashMap<>());
        phone.get(firstChar).put(name, number);
    }

    public Long getContect(String name){
        Character firstChar = Character.toUpperCase(name.charAt(0)); 
        return phone.get(firstChar).get(name);

    }

}
