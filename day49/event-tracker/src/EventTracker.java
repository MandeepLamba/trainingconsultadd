import java.util.HashMap;
import java.util.Map;

public class EventTracker {
    private static Map<String, Object> details = new HashMap<>();

    public static void addDetails(String key, Object value){
        details.put(key, value);
    }

    public static Map<String, Object> getDetails(){
        return details;
    }

}
