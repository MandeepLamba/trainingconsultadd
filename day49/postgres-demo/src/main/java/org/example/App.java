package org.example;

import org.example.model.Student;

import java.sql.*;

/**
 * Hello world!
 *
 */
public class App 
{
    private static final String DB_URL = "jdbc:postgresql:mandeep";
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        Student student = new Student(24,"Param");
        System.out.println("Generated ID is : " + insert(student));
    }

    private static long insert(Student student) {
       try(PreparedStatement ps = connect().prepareStatement("insert into student values(?,?)", Statement.RETURN_GENERATED_KEYS)){
           ps.setInt(1,student.getId());
           ps.setString(2,student.getName());

           int rowsEffected = ps.executeUpdate();
           if(rowsEffected>0){
               System.out.println(rowsEffected + " rows effected!");
               try(ResultSet rs = ps.getGeneratedKeys()){
                   if(rs.next()){
                       return rs.getLong(1);
                   }
               }
           }
       } catch (SQLException throwables) {
           throwables.printStackTrace();
       }
        return -1;
    }

    public static Connection connect() throws SQLException {
        return DriverManager.getConnection(DB_URL, "postgres", "postgres");
    }
}
