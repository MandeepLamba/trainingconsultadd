package org.example;


import com.Module1Class;

import javax.naming.Context;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Hello world!
 *
 */
public class App 
{
    private static final String DB_URL = "jdbc:postgresql:mandeep";

    public static void main( String[] args )
    {
        System.out.println("Hello " + Module1Class.name);
        try {
            connect();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    public static Connection connect() throws SQLException {
        return DriverManager.getConnection(DB_URL, "postgres", "postgres");
    }
}
