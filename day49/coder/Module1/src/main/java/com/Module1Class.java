package com;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Module1Class {
    public static final String name= "Module1Class";
    private static final String DB_URL = "jdbc:postgresql:mandeep";

    public static void main(String[] args) {
        try {
            System.out.println(connect().toString());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static Connection connect() throws SQLException {
        return DriverManager.getConnection(DB_URL, "postgres", "postgres");
    }
}
