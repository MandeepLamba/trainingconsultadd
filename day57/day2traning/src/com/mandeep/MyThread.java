package com.mandeep;

public class MyThread extends Thread{

    private int start,end;

   public MyThread(int start, int end){
       this.start = start;
       this.end = end;
   }
    @Override
    public void run() {
        System.out.println("MyThread is running now");

        for (int i =start; i<end ; i++ ) {
            try {
                System.out.println("Thread Is running : " + i);
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
