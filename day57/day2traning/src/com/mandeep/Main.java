package com.mandeep;


import java.io.*;
import java.net.URL;

public class Main {

    public static void main(String[] args) {
	// write your code here
        System.out.println("Hello");

//        List<String> strings = new ArrayList<>();
//        strings.add("345");
//        strings.add("234");
//        strings.add("34a");
//        strings.add("356");
//        strings.add("432");

        Thread t = new Thread(()->{
            try {
                InputStreamReader is = new InputStreamReader(new URL("http://www.google.com").openStream());
                BufferedReader bs = new BufferedReader(is);
                System.out.println(bs.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        t.start();


        MyThread thread = new MyThread(10,15);
        thread.setPriority(8);


        System.out.println("Main Thread Ends");
    }
}
