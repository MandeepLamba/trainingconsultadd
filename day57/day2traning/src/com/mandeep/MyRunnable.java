package com.mandeep;

public class MyRunnable implements Runnable{

    private int count;
    public MyRunnable(int i) {
        count = i;
    }

    @Override
    public void run() {
        System.out.println("MyRunnable is running now");

        for (int i =0; i<count ; i++ ) {
            try {
                Thread.sleep(200);
                System.out.println("Runnable Is running : " + i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
