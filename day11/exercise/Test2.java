class Machine {
    public String type;
    public Machine(){
        type = "Machine";
    }
    public void start(){
        System.out.println("Machine is Working!");
    }
}

class Car extends Machine {
    public String type;
    public Car(){
        type = "Car";
    }
    
    public void start(){

        System.out.println("Car is Working!");

    }
}


public class Test2 {

    public static void main(String[] args) {
        Car car = new Car();
        Machine machine = new Car();

        System.out.println(car.type);
        System.out.println(machine.type);

        car.start();
        machine.start();
    }
}