package com.example.mqpoc.consumer;

import com.example.mqpoc.model.Message;
import com.ibm.mq.jms.MQQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;

@Component
public class MessageConsumer {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageConsumer.class);

    @JmsListener(destination = "DEV.QUEUE.1", containerFactory = "ibmMqFactory")
    public void logMessageFromIBMMQ1(Message message) {
        LOGGER.info("IBM M Queue 1 : Message Received {}",message);
    }
    @JmsListener(destination = "DEV.QUEUE.2", containerFactory = "ibmMqFactory")
    public void logMessageFromIBMMQ2(Message message) {
        LOGGER.info("IBM M Queue 2 : Message Received {}",message);
    }
    @JmsListener(destination = "DEV.QUEUE.3", containerFactory = "ibmMqFactory")
    public void logMessageFromIBMMQ3(Message message) {
        LOGGER.info("IBM M Queue 3 : Message Received {}",message);
    }

    @JmsListener(destination = "queue1", containerFactory = "mqFactory")
    public void logMessage(Message message) {
        LOGGER.info("Active M Queue 1 : Message Received {}",message);
    }
    @JmsListener(destination = "queue2", containerFactory = "mqFactory")
    public void logMessage1(Message message) {
        LOGGER.info("Active M Queue 2 : Message Received {}",message);
    }
    @JmsListener(destination = "queue3", containerFactory = "mqFactory")
    public void logMessage2(Message message) {
        LOGGER.info("Active M Queue 3 : Message Received {}",message);
    }
}
