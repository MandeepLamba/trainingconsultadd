package com.example.mqpoc.publish;

import com.example.mqpoc.model.Message;
import com.example.mqpoc.model.MessageRequest;
import com.ibm.mq.jms.MQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
public class PublishController {

    @Autowired
    private JmsTemplate jmsTemplate;

    @PostMapping("/activemq")
    public ResponseEntity<String> enqueueActiveMQ(@RequestBody MessageRequest messageRequest) {
        try{
            jmsTemplate.convertAndSend(messageRequest.getQueue(),messageRequest.getMessage());
            return new ResponseEntity<>("Sent",HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping("/ibmmq")
    public ResponseEntity<String> enqueueIBMMQ(@RequestBody MessageRequest messageRequest) {
        try{
            MQQueue mqQueue = new MQQueue("DEV.QUEUE.1");
            for (int i = 0; i < 20; i++) {
                messageRequest.getMessage().setMsg(messageRequest.getMessage().getMsg()+i);
                jmsTemplate.convertAndSend(mqQueue,messageRequest.getMessage());
            }

            return new ResponseEntity<>("Sent",HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
