package com.example;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

public class ParallelStreamEx {
    private static int count= 50000;
    public static void main(String[] args) {
        System.out.println(test( ParallelStreamEx::sortParallel,50));
        System.out.println(test(ParallelStreamEx::sortSq,50));
    }

    public static long test(Supplier<Long> supplier , long loop){
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < loop; i++) {
            supplier.get();
        }
        return System.currentTimeMillis()-startTime;
    }

    public static long sortParallel(){
        List<RandomToken> randomTokens = LongStream.rangeClosed(0, count)
                .parallel()
                .mapToObj(i -> new RandomToken(i, ThreadLocalRandom.current().nextLong()))
                .collect(Collectors.toList());
        randomTokens.stream().parallel().sorted(Comparator.comparing(RandomToken::getToken));
        return 2;
    }
    public static long sortSq(){
        List<RandomToken> randomTokens = LongStream.rangeClosed(0, count)
                .mapToObj(i -> new RandomToken(i, ThreadLocalRandom.current().nextLong()))
                .collect(Collectors.toList());
        randomTokens.stream().sorted(Comparator.comparing(RandomToken::getToken));
        return 1;
    }
}

class RandomToken{
    long id;
    long token;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getToken() {
        return token;
    }

    public void setToken(long token) {
        this.token = token;
    }

    public RandomToken(long id, long token) {
        this.id = id;
        this.token = token;
    }
}
