package com.example;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PartitionByEx {
    public static void main(String[] args) {
        //partitioningBy with one parameter
        Predicate<Teacher> pr = teacher -> teacher.getAge()>30;
        Map<Boolean, List<Teacher>> part = Teachers.getAll().stream()
                .collect(Collectors.partitioningBy(pr));
        part.forEach((k,v)-> System.out.println(k + " : " + v));


        System.out.println("--------");
        //partitioningBy with two parameter
        Map<Boolean, Set<Teacher>> part2 = Teachers.getAll().stream()
                .collect(Collectors.partitioningBy(pr,Collectors.toSet()));
        part.forEach((k,v)-> System.out.println(k + " : " + v));

    }
}
