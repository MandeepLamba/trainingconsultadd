package com.example;

import java.util.Arrays;
import java.util.List;

public class Teachers {
    public static List<Teacher> getAll(){
        Teacher t1 = new Teacher(
                "Mike",
                true,
                "M",
                23,
                Arrays.asList("Java","Python")
        );
        Teacher t2 = new Teacher(
                "Bob",
                false,
                "M",
                31,
                Arrays.asList("English","C++","Java")
        );
        Teacher t3 = new Teacher(
                "Jenny",
                true,
                "F",
                23,
                Arrays.asList("C","HTML","SQL","Swift")
        );
        Teacher t4 = new Teacher(
                "Reena",
                false,
                "F",
                43,
                Arrays.asList("CSS","JavaScript")
        );
        Teacher t5 = new Teacher(
                "Mark",
                true,
                "M",
                31,
                Arrays.asList("Java", "Hadoop","Node.js","Angular.js")
        );
        Teacher t6 = new Teacher(
                "Selena",
                true,
                "F",
                38,
                Arrays.asList("Coral Draw","Blander", "Autocad")
        );
        return Arrays.asList(t1,t2,t3,t4,t5,t6);

    }
}
