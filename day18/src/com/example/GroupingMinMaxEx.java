package com.example;

import java.util.IntSummaryStatistics;
import java.util.Map;
import java.util.stream.Collectors;

public class GroupingMinMaxEx {
    public static void main(String[] args) {
        //SummaryStatistics
        Map<Boolean, IntSummaryStatistics> statisticsMap = Teachers.getAll().stream()
                .collect(Collectors.groupingBy(Teacher::isTeachOnline,
                        Collectors.summarizingInt(Teacher::getAge)));

        statisticsMap.forEach((k,v)-> System.out.println(k + " : " + v));
    }


}
