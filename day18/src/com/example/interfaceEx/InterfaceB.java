package com.example.interfaceEx;

public interface InterfaceB {
    default void sumA(int num1, int num2){
        System.out.println("SumB: " + num1 + num2);
    }
}
