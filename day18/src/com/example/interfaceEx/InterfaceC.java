package com.example.interfaceEx;

public interface InterfaceC {
    default void sumC(int num1, int num2){
        System.out.println("SumC: " + num1 + num2);
    }

}
