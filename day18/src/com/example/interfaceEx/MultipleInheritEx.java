package com.example.interfaceEx;

public class MultipleInheritEx implements InterfaceA,InterfaceB,InterfaceC{
    public static void main(String[] args) {
        MultipleInheritEx multipleInheritEx = new MultipleInheritEx();
        multipleInheritEx.sumA(3,4);
    }


    @Override
    public void sumA(int num1, int num2) {
        System.out.println("MultipleInheritEx.sumA: " + num1+num2);
    }
}
