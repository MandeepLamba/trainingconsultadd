package com.example.interfaceEx;

@FunctionalInterface
public interface Calculator {

    //abstract methods
    int sum(int a, int b);
//    int div(int a, int b);

    //default methods
    default int sub(int num1, int num2){
        return num1-num2;
    }

    //static methods
    static int mul(int num1, int num2){
        return num1 * num2;
    }
}
