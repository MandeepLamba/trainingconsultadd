package com.example.interfaceEx;

public class CalculatorApp implements Calculator{

    public static void main(String[] args) {
        System.out.println(new CalculatorApp().sum(1,2));
//        System.out.println(new CalculatorApp().div(5,2));

        Calculator calculator = (a,b) -> a+b;
        System.out.println(calculator.sum(2,7));
        System.out.println(calculator.sub(7,3));
        System.out.println(Calculator.mul(3,9));
    }

    @Override
    public int sum(int a, int b) {
        return a+b;
    }

}
