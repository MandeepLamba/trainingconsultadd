package com.example.interfaceEx;

public interface InterfaceA {
    default void sumA(int num1, int num2){
        System.out.println("SumA: " + num1 + num2);
    }
}
