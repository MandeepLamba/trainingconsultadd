package com.example;

import java.util.List;

public class Teacher {
    private String name;
    private boolean teachOnline;
    private String gender;
    private int age;
    private List<String> courses;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "name='" + name + '\'' +
                ", teachOnline=" + teachOnline +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                ", courses=" + courses +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isTeachOnline() {
        return teachOnline;
    }

    public void setTeachOnline(boolean teachOnline) {
        this.teachOnline = teachOnline;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<String> getCourses() {
        return courses;
    }

    public void setCourses(List<String> courses) {
        this.courses = courses;
    }

    public Teacher(String name, boolean teachOnline, String gender, int age, List<String> courses) {
        this.name = name;
        this.teachOnline = teachOnline;
        this.gender = gender;
        this.age = age;
        this.courses = courses;
    }
}
