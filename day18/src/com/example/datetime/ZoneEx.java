package com.example.datetime;

import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ZoneEx {
    public static void main(String[] args) {
        ZoneId.getAvailableZoneIds().stream()
                .filter(s -> s.startsWith("Asia"))
                .sorted().forEach(System.out::println);
        System.out.println("Asia/Dili: " + ZonedDateTime.now(ZoneId.of("Asia/Dili")));
        System.out.println(ZoneId.systemDefault());
    }
}
