package com.example.datetime;

import java.time.LocalDate;
import java.time.temporal.ChronoField;

public class LocalDateEx {
    public static void main(String[] args) {
        //creating
        LocalDate localDate = LocalDate.now();
        System.out.println("localDate = " + localDate);

        localDate = LocalDate.ofYearDay(2019,67);
        System.out.println("localDate = " + localDate);

        localDate = LocalDate.of(2019,3,23);
        System.out.println("localDate = " + localDate);

        //getting
        System.out.println("localDate.getMonth() = " + localDate.getMonth());
        System.out.println("localDate.getMonthValue() = " + localDate.getMonthValue());
        
        //using constants
        System.out.println("localDate.get(ChronoField.DAY_OF_WEEK) = " + localDate.get(ChronoField.DAY_OF_WEEK));
        
        //modifying

        System.out.println("localDate.plusDays(5) = " + localDate.plusDays(5));
        System.out.println("localDate.plusMonths(2) = " + localDate.plusMonths(2));
        System.out.println("localDate.withYear(2025) = " + localDate.withYear(2025));



    }
}
