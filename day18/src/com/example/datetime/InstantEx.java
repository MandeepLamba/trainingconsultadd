package com.example.datetime;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class InstantEx {
    public static void main(String[] args) {
        Instant instant = Instant.now();
        System.out.println("instant = " + instant);
        Instant instant1 = instant.plusSeconds(34000);
        Duration duration = Duration.between(instant,instant1);
        System.out.println("duration.getNano() = " + duration.getSeconds());
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant1, ZoneId.systemDefault());
        System.out.println("localDateTime = " + localDateTime);
    }
}
