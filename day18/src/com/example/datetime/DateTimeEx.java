package com.example.datetime;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.Calendar;
import java.util.Date;

public class DateTimeEx {
    public static void main(String[] args) {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        System.out.println("dateFormat.format(date) = " + dateFormat.format(date));
        dateFormat = new SimpleDateFormat("HH:mm:ss");
        System.out.println("dateFormat.format(date) = " + dateFormat.format(date));

        System.out.println(Calendar.getInstance().getTime());


        //LocalDate & Time
        LocalDate localDate = LocalDate.now();
        System.out.println("localDate = " + localDate);

        LocalTime localTime = LocalTime.now();
        System.out.println("localTime = " + localTime);

        LocalDateTime dateTime = LocalDateTime.now();
        System.out.println("dateTime = " + dateTime);
        
        //duration
        
        LocalDateTime dateTime1 = LocalDateTime.of(2021,5,30,0,0,0);
        Duration duration = Duration.between(dateTime,dateTime1);
        System.out.println("duration.toDays() = " + duration.toDays());
        System.out.println("duration.toDays() = " + duration.toHours());


        //Date to LocalDateTime
        System.out.println("Date to LocalDateTime");
        System.out.println("Date: " + date);
        System.out.println("LocalDateTime: " + date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());

    }
}
