package com.example.datetime;

import java.time.LocalTime;
import java.time.temporal.ChronoField;

public class LocalTimeEx {
    public static void main(String[] args) {
        //creating
        LocalTime localTime = LocalTime.now();
        System.out.println("localTime = " + localTime);
        
        localTime = LocalTime.of(13,45);
        System.out.println("localTime = " + localTime);
        
        localTime = LocalTime.of(14, 52,12);
        System.out.println("localTime = " + localTime);

        //get
        System.out.println("localTime.getHour() = " + localTime.getHour());
        System.out.println("localTime.getMinute() = " + localTime.getMinute());
        System.out.println("localTime.getSecond() = " + localTime.getSecond());

        System.out.println("---------");
        System.out.println("localTime = " + localTime);
        System.out.println("localTime.with(ChronoField.HOUR_OF_DAY,22) = " + localTime.with(ChronoField.HOUR_OF_DAY,22));
        
    }
}
