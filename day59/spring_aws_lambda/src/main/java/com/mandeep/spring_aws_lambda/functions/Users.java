package com.mandeep.spring_aws_lambda.functions;

import com.mandeep.spring_aws_lambda.model.User;
import com.mandeep.spring_aws_lambda.repo.UserDao;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Users implements Function<String, List<User>> {

    @Autowired
    UserDao userDao;

    @Override
    public List<User> apply(String s) {
        return userDao.getUsers().stream().filter((user) -> user.getClassName().equals(s)).collect(Collectors.toList());
    }
}
