package com.mandeep.spring_aws_lambda.model;


public class User {
    private int id;
    private String name;
    private String className;
    
    
    public User() {
    	
    }
    
    
	public User(int id, String name, String className) {
		super();
		this.id = id;
		this.name = name;
		this.className = className;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the className
	 */
	public String getClassName() {
		return className;
	}
	/**
	 * @param className the className to set
	 */
	public void setClassName(String className) {
		this.className = className;
	}
    
    
}
