package com.mandeep.spring_aws_lambda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.function.Function;

@SpringBootApplication
public class SpringAwsLambdaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringAwsLambdaApplication.class, args);
	}

	@Bean
	public Function<String, String> reverseString(){
		return (input) -> new StringBuilder(input).reverse().toString();
	}

}
