package com.mandeep.spring_aws_lambda.repo;

import com.mandeep.spring_aws_lambda.model.User;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
public class UserDao {

    public List<User> getUsers(){
        return Arrays.asList(
                new User(23,"Mandeep","MCA"),
                new User(45,"Param","MCA"),
                new User(22,"Sonu","MCA"),
                new User(11, "Kuldeep","BCA"),
                new User(56,"Anuj","BCA")
        );
    }
}
