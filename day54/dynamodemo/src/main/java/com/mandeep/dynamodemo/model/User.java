package com.mandeep.dynamodemo.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import java.io.Serializable;


@DynamoDBTable(tableName = "Users")
public class User implements Serializable {

    private String userId;
    private String classname;
    private String name;
    private int passout;
    private Address address;

    @DynamoDBAttribute(attributeName = "address")
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @DynamoDBHashKey(attributeName = "UserID")
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }



    @DynamoDBAttribute(attributeName = "class")
    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    @DynamoDBAttribute(attributeName = "name")
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "User{" +
                "UserID='" + userId + '\'' +
                ", classname='" + classname + '\'' +
                ", name='" + name + '\'' +
                ", passout=" + passout +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    @DynamoDBAttribute(attributeName = "passout")
    public int getPassout() {
        return passout;
    }

    public void setPassout(int passout) {
        this.passout = passout;
    }
}
