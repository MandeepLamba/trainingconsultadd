package com.mandeep.dynamodemo;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.mandeep.dynamodemo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DynamodemoApplication {

	@Autowired
	private DynamoDBMapper dynamoDBMapper;

	public static void main(String[] args) {
		SpringApplication.run(DynamodemoApplication.class, args);

	}

	@GetMapping("/")
	public String home(){
		return "Welcome to Dynamo App";
	}

	@PostMapping(value = "/get")
	public String getUsers(@RequestParam String id){
		User user = dynamoDBMapper.load(User.class,id);
		System.out.println(user);
		return user.toString();
	}

	@PostMapping(value = "/put")
	public ResponseEntity<String> putUser(@RequestBody User user){
		dynamoDBMapper.save(user);
		return ResponseEntity.accepted().body("Successfully");
	}

}
