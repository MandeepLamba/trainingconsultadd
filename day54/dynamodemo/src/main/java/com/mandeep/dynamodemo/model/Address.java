package com.mandeep.dynamodemo.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;

@DynamoDBDocument
public class Address {
    private String firstLine;
    private String SecondLine;
    private long areaCode;
    private String city;
    private String state;
    private String country;

    @DynamoDBAttribute(attributeName = "firstLine")
    public String getFirstLine() {
        return firstLine;
    }

    public void setFirstLine(String firstLine) {
        this.firstLine = firstLine;
    }

    @DynamoDBAttribute(attributeName = "secondLine")
    public String getSecondLine() {
        return SecondLine;
    }

    public void setSecondLine(String secondLine) {
        SecondLine = secondLine;
    }

    @DynamoDBAttribute(attributeName = "areaCode")
    public long getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(long areaCode) {
        this.areaCode = areaCode;
    }

    @DynamoDBAttribute(attributeName = "cityName")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @DynamoDBAttribute(attributeName = "state")
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @DynamoDBAttribute(attributeName = "country")
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
