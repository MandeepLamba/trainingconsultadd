package com.example;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class MapTo {

    public static void main(String[] args) {
        IntStream integers = IntStream.rangeClosed(1,10);

        //map to Object
        List<ListRandom> list = integers.mapToObj(i-> new ListRandom(i,
                ThreadLocalRandom.current().nextInt(100))).collect(Collectors.toList());
        list.forEach(System.out::println);

        //map to Double

        IntStream.rangeClosed(1,10).mapToDouble(i->(double) i).forEach(System.out::println);


    }
}

class ListRandom{
    int id;
    int random;

    public ListRandom(int id, int random) {
        this.id = id;
        this.random = random;
    }

    @Override
    public String toString() {
        return "ListRandom{" +
                "id=" + id +
                ", random=" + random +
                '}';
    }
}