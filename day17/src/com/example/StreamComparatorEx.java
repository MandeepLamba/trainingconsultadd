package com.example;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class StreamComparatorEx {
    public static void main(String[] args) {
        List<Teacher> teachers = Teachers.getAll();

        System.out.println(teachers.stream().map(Teacher::getName).sorted().collect(Collectors.toList()));
        List<Teacher> list1 = teachers.stream()
                .sorted(Comparator.comparing(Teacher::getName))
                .filter(Teacher::isTeachOnline)
                .collect(Collectors.toList());

        list1.forEach(System.out::println);



    }
}
