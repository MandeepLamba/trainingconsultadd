package com.example;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CollectorsMethodEx {

    public static void main(String[] args) {

        //joining
        System.out.println("Joining ---------");
        System.out.println(Stream.of("M","A","N","D").collect(Collectors.joining()));
        System.out.println(Teachers.getAll().stream()
                .map(Teacher::getName)
                .collect(Collectors.joining(", ","{","}")));

        //mapping
        System.out.println("Mapping ----------");
        System.out.println(Teachers.getAll().stream().map(Teacher::getName).collect(Collectors.toList()));
        System.out.println(Teachers.getAll()
                .stream()
                .collect(Collectors.mapping(Teacher::getName, Collectors.toList())));

        //grouping by
        System.out.println("Grouping By ---------");
        Map<Integer, List<String>> groupAge = Teachers.getAll().stream()
                .collect(Collectors.groupingBy(Teacher::getAge,
                        Collectors.mapping(Teacher::getName,Collectors.toList())));
        groupAge.forEach((k,v)-> System.out.println(k + " : " + v));

        //min by and max by
        System.out.println("min & max -------");
        System.out.println("Max Aged: d" + Teachers.getAll().stream().collect(Collectors.maxBy(Comparator.comparing(Teacher::getAge))).get());
        System.out.println("Min Aged: d" + Teachers.getAll().stream().collect(Collectors.minBy(Comparator.comparing(Teacher::getAge))).get());

        //summing and averaging
        System.out.println("Summing and Averaging -------");
        System.out.println("Sum of ages: " + Teachers.getAll().stream().collect(Collectors.summingInt(Teacher::getAge)));
        System.out.println("Average of ages: " + Teachers.getAll().stream().collect(Collectors.averagingInt(Teacher::getAge)));

    }
}
