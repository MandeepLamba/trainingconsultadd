package com.example;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamFactoryMethods {
    public static void main(String[] args) {
        //of
        IntStream integerStream = IntStream.of(2,3,4,5,6,7,8);
        integerStream.forEach(System.out::println);

        //iterate
        System.out.println("--------");
        Stream<Integer> integerStream1 = Stream.iterate(2,a->a+2).limit(7);
        integerStream1.forEach(System.out::println);

        //generate
        System.out.println("--------");
        Stream<Integer> integerStream2 = Stream.generate(new Random()::nextInt).limit(5);
        integerStream2.forEach(System.out::println);

        //aggregate
        System.out.println(IntStream.rangeClosed(2,6).sum());
        System.out.println(IntStream.rangeClosed(2,6).min());
        System.out.println(IntStream.rangeClosed(2,6).max());
        System.out.println(IntStream.rangeClosed(2,6).average());


        //IntStream to List<Integer> and visa versa
        IntStream is = IntStream.rangeClosed(1,10);
        List<Integer> nums = is.boxed().collect(Collectors.toList());

        is = nums.stream().mapToInt(Integer::intValue);
        is.forEach(System.out::println);


    }

}
