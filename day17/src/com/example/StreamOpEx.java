package com.example;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class StreamOpEx {
    public static void main(String[] args) {
        System.out.println("Main Started");
        List<Teacher> teachers = Teachers.getAll();

        //flatMap method
        List<String> list = teachers.stream()
                .map(Teacher::getCourses)
                .flatMap(List::stream)
                .collect(Collectors.toList());
        System.out.println(list);

        //distinct method
        List<String> list1 = teachers.stream()
                .map(Teacher::getCourses)
                .flatMap(List::stream)
                .distinct()
                .collect(Collectors.toList());
        System.out.println(list1);

        //count
        Long count = teachers.stream()
                .map(Teacher::getCourses)
                .flatMap(List::stream)
                .distinct().count();
        System.out.println("Total available subjects: " + count);

        //sorted
        List<String> list3 = teachers
                .stream()
                .map(Teacher::getCourses)
                .flatMap(List::stream)
                .distinct()
                .sorted()
                .collect(Collectors.toList());
        System.out.println(list3);

        //match
        System.out.println(teachers.stream().map(Teacher::getCourses).flatMap(List::stream).allMatch(s -> s.startsWith("C")));
        System.out.println(teachers.stream().map(Teacher::getCourses).flatMap(List::stream).anyMatch(s -> s.startsWith("C")));
        System.out.println(teachers.stream().map(Teacher::getCourses).flatMap(List::stream).noneMatch(s -> s.startsWith("C")));


        //reduce
         Optional<Teacher> all = teachers.stream().reduce((teacher, teacher2) -> teacher.getAge()>teacher2.getAge()?teacher:teacher2);
        System.out.println(all);

        //Average of ages of all teachers

        System.out.println("------------");

        teachers.forEach(System.out::println);

        int avg = teachers.stream()
                .filter( teacher -> teacher.isTeachOnline())
                .map(teacher -> teacher.getAge())
                .reduce((a,b)->a+b)
                .get()/teachers.size();

        System.out.println(avg);

        //min max
        List<Integer> list2 = Arrays.asList(2,3,4,5,6,7,8,9);
        Optional res = list2.stream().max(Integer::compareTo);
        System.out.println(res.get());
        res = list2.stream().min(Integer::compareTo);
        System.out.println(res.get());

        res =list2.stream().reduce(Integer::max);
        System.out.println(res.get());

        //find
        System.out.println(teachers.stream().findAny().get());
        System.out.println(teachers.stream().findFirst().get());
    }
}
