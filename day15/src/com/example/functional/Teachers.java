package com.example.functional;

import java.util.Arrays;
import java.util.List;

public class Teachers {
    public static List<Teacher> getAll(){
        Teacher t1 = new Teacher(
                "Mike",
                true,
                "M",
                Arrays.asList("Java","Python")
        );
        Teacher t2 = new Teacher(
                "Bob",
                false,
                "M",
                Arrays.asList("English","C++")
        );
        Teacher t3 = new Teacher(
                "Jenny",
                true,
                "F",
                Arrays.asList("C","HTML","SQL")
        );
        Teacher t4 = new Teacher(
                "Reena",
                false,
                "F",
                Arrays.asList("CSS","JavaScript")
        );
        Teacher t5 = new Teacher(
                "Mark",
                true,
                "M",
                Arrays.asList("Hadoop","Node.js")
        );
        return Arrays.asList(t1,t2,t3,t4,t5);

    }
}
