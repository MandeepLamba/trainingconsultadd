package com.example.functional;

import java.util.List;

public class Teacher {
    private String name;
    private Boolean teachOnline;
    private String gender;
    private List<String> courses;

    @Override
    public String toString() {
        return "Teacher{" +
                "name='" + name + '\'' +
                ", teachOnline=" + teachOnline +
                ", gender='" + gender + '\'' +
                ", courses=" + courses +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getTeachOnline() {
        return teachOnline;
    }

    public void setTeachOnline(Boolean teachOnline) {
        this.teachOnline = teachOnline;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<String> getCourses() {
        return courses;
    }

    public void setCourses(List<String> courses) {
        this.courses = courses;
    }

    public Teacher(String name, Boolean teachOnline, String gender, List<String> courses) {
        this.name = name;
        this.teachOnline = teachOnline;
        this.gender = gender;
        this.courses = courses;
    }
}
