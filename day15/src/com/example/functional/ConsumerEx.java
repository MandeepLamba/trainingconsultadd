package com.example.functional;

import java.util.List;
import java.util.function.*;

public class ConsumerEx {
    public static void main(String[] args) {
        Consumer<String> con = (x) -> System.out.println("Length of: " + x + " is: " + x.length());
        con.accept("Time to fly");

        //Consumer with Block
        Consumer<Integer> conInt = (x) -> {
            System.out.println(x + "*" + x + " = " + x * x);
            System.out.println(x + "/" + x + " = " + x / x);
        };
        conInt.accept(4);


        List<Teacher> teachers = Teachers.getAll();

        //Iterating through teachers
        Consumer<Teacher> c = t -> System.out.println(t);

        //or with method reference
//        Consumer<Teacher> c = System.out::println;
        teachers.forEach(c);

        //Printing Names of Teachers
        Consumer<Teacher> tc = teacher -> System.out.println(teacher.getName());
        teachers.forEach(tc);

        //printing courses of each teacher
        Consumer<Teacher> tc1 = teacher -> System.out.println(teacher.getCourses());
        teachers.forEach(tc.andThen(tc1));

        //printing only female teachers
        teachers.forEach((teacher -> {
            if(teacher.getGender().equals("F")) System.out.println(teacher.getName());
        }));

        //printing male teacher who teach online
        System.out.println("---------------");
        teachers.forEach(teacher -> {
            if(teacher.getGender().equals("M") && teacher.getTeachOnline()){
                tc.andThen(tc1).accept(teacher);
            }
        });


        //different Consumers

        IntConsumer ic = value -> System.out.println(value * 4);
        ic.accept(5);

        LongConsumer lc = value -> System.out.println(value * 33);
        lc.accept(3L);

        DoubleConsumer dc = value -> System.out.println(value * 3);
        dc.accept(34.33);

        BiConsumer<String ,Integer> bc = (name,age) -> System.out.println(name + " is " + age + " years old!");
        bc.accept("Mandeep",24);

        //BiConsumer for printing teachers name and gender

        BiConsumer<String,String> tbc = (name,gender) -> System.out.println(name + " is " + (gender.equals("M")?"Male":"Female") + "!");;
        teachers.forEach(teacher -> tbc.accept(teacher.getName(),teacher.getGender()));
    }
}
