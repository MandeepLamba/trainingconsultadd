package com.example;

public class HelloOld implements HelloInterface, IncInterface, ConcatInterface {
    @Override
    public String sayHello() {
        return "Hello";
    }


    public static void main(String[] args) {
        HelloOld helloOld = new HelloOld();
        System.out.println(helloOld.sayHello());

        System.out.println(helloOld.inc(5));

        System.out.println(helloOld.concat("Mandeep", "Lamba"));
    }

    @Override
    public int inc(int a) {
        return a + 1;
    }

    @Override
    public String concat(String a, String b) {
        return a + " " + b;
    }
}
