package com.example;

import java.util.stream.IntStream;

public class RunnableEx {
    public static void main(String[] args) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 5; i++) {
                    System.out.println(i);
                }
            }
        };
        new Thread(runnable).start();

        //using lambda
        new Thread(()->{
            for (int i = 0; i < 5; i++) {
                System.out.println(i);
            }
        }).start();
    }
}
