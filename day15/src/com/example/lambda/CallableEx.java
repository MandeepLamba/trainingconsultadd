package com.example;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.IntStream;

public class CallableEx {

    private static int[] array = IntStream.rangeClosed(1,1000).toArray();
    private static int total = Arrays.stream(array).sum();

    public static void main(String[] args) throws InterruptedException, ExecutionException {

        Callable callable = () -> {
            int sum = 0;
            for (int i = 0; i < array.length / 2; i++) {
                sum += array[i];
            }
            return sum;
        };
        Callable callable1 = () -> {
            int sum = 0;
            for (int i = array.length / 2; i < array.length; i++) {
                sum += array[i];
            }
            return sum;
        };

        ExecutorService executorService = Executors.newFixedThreadPool(2);
        List<Callable<Integer>> callables = Arrays.asList(callable, callable1);
        List<Future<Integer>> futures = executorService.invokeAll(callables);


        int sum =0;
        for (Future<Integer> f :
                futures) {
            sum += f.get();
            System.out.println("Sum is: " + f.get());
        }
        System.out.println("Calculated Total = " + sum);
        System.out.println("Total should be = " + total);

    }
}
