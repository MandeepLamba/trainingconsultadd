package com.example;

public class HelloLambda {
    public static void main(String[] args) {
        HelloInterface helloInterface = () -> "Hello Lambda";
        IncInterface incInterface = (a) -> 3+a;
        ConcatInterface concatInterface = (a,b) -> a + " " + b;
        System.out.println(helloInterface.sayHello());
        System.out.println(incInterface.inc(6));
        System.out.println(concatInterface.concat("Concat", "Lambda"));

    }
}
