package org.example;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.VoidFunction;

import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        SparkConf sparkConf = new SparkConf().setAppName("sparkdemo").setMaster("local[*]");
        JavaSparkContext context = new JavaSparkContext(sparkConf);
        List<String> st = new ArrayList<>();
        for(String s : "/Users/mandeeplamba/Documents/training/trainingconsultadd/day56/sparkdemo/src/main/resources/exam.txt".split("/")){
            st.add(s);
        }
        JavaRDD<String> textFile = context.parallelize(st);


        textFile.foreach(line -> System.out.println("* "+line));
        System.out.println(textFile.collect());
    }
}
