package com.mandeep;

import java.util.ArrayList;
import java.util.List;

public class MyList<Type> {

    List<Type> list = new ArrayList<>();

    public void add(Type value){
        list.add(value);
    }

    public List<Type> getList(){
        return list;
    }

    @Override
    public String toString() {
        return "MyList contains List : {" +
                "list=" + list +
                '}';
    }
}
