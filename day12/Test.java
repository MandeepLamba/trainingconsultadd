
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Test {

    public static void main(String[] args) {

        //Reading File
        
        File file = new File("day12/doc.txt");
        try(BufferedReader br = new BufferedReader(new FileReader(file))){
            String line;
            while ((line =br.readLine())!=null) {
            System.out.println(line);
                
            }
        } catch(FileNotFoundException e){
            System.out.println("File not found!");
        } catch(IOException e){
            System.out.println("Unable to read!");
        } 

        //write to file
        file = new File("day12/write.txt");
        try(BufferedWriter br = new BufferedWriter(new FileWriter(file))){
            br.write("line one");
            br.newLine();
            br.write("line two.");
            br.newLine();
            br.write("Third Line Appended");
        } catch(IOException e){
            System.out.println("Unable to read!");
        } 

        //enum
        System.out.println(Color.RED.getName());
        System.out.println(Color.RED);


        //Writing objects to files:

        Person p1 = new Person("Joy",23);
        Person p2 = new Person("Rachel", 23);

        try (FileOutputStream fos = new FileOutputStream("day12/data.bin")) {
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(p1);
            oos.writeObject(p2);
            oos.close();
        } catch (IOException e) {
            System.out.println("Unable to write! " + e);
        }
        
        //Reading objects to files:

        try (FileInputStream fis = new FileInputStream("day12/data.bin")) {
            ObjectInputStream oos = new ObjectInputStream(fis);
            p1 = (Person)oos.readObject();
            p2 = (Person)oos.readObject();

            System.out.println(p1 + "\n" + p2);
            oos.close();
        } catch (IOException e) {
            System.out.println("Unable to write! " + e);
        } catch(ClassNotFoundException e){
            System.out.println("Object type mismatch");
        }

    }

}

class Person implements Serializable{
    private String name;
    private int id;

    public Person(String name , int id){
        this.name=name;
        this.id=id;
    }

    @Override
    public String toString(){
        return "Name: " + this.name + " and ID: " + this.id;
    }
}