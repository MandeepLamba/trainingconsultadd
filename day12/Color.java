public enum Color {
    RED("#ff0000"),GREEN("#00ff00"),BLACK("#000000");
    private String name;
    Color(String name){
        this.name=name;
    }
    public String getName() {
        return name;
    }
}
