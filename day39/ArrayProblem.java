package day39;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrayProblem {

    public static void main(String[] args) {

        // int[] a = new int[]{3,1,3,5,10,6,4,3,1};
        // int[] b = new int[]{5,2,4,6};
        // int[] c = new int[]{3,0,0,-2,0,2,0,-1};
        // System.out.println(movMedian(a));


        Double d = 33424234.546464655;

        System.out.println(String.format("%,.4f", d));


    }















    public static String movMedian(int[] arr) {

        List<Integer> res = new ArrayList<>();
        
        int windowSize = arr[0];
        arr = Arrays.copyOfRange(arr, 1,arr.length);

        for (int i = 0; i < arr.length; i++) {

            int[] window;

            if(i < windowSize-1) window = Arrays.copyOfRange(arr, 0, i+1);
            else  window = Arrays.copyOfRange(arr, i-2, i+1); 

            Arrays.sort(window);
                if(window.length%2 == 0){
                    res.add((window[(window.length-1)/2] + window[(window.length+1)/2])/2);
                }
                else{
                    res.add(window[(window.length/2)]);
                }
        }
        return res.toString();

    }
}