interface Day14Interface {
    public int run(int a);
}

class Day14Class {
    void execute(Day14Interface e,int a){
        System.out.println("executing Code");
        int value = e.run(a);
        System.out.println("We got value from run= " + value);
    }
    void execute(Day14Interface e){
        System.out.println("executing Code");
        int value = e.run(10);
        System.out.println("We got value from run= " + value);
    }
}
public class Day14 {
    public static void main(String[] args) {
        Day14Class class1 = new Day14Class();
        class1.execute(new Day14Interface(){
            @Override
            public int run(int a) {
                System.out.println("Hello World!");
                return a+2;
            }
        },4);
        System.out.println();
        //using lambda
        class1.execute((a)->{
            System.out.println("Hello World using Lambda!");
            return a+2;
        },6);

        System.out.println();

        //using lambda
        class1.execute((a)-> a+2,8);
        class1.execute((a)-> a+3);

    }
}
