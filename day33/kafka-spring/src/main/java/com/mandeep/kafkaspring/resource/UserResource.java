package com.mandeep.kafkaspring.resource;

import com.mandeep.kafkaspring.model.User;
import com.mandeep.kafkaspring.model.emp;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.record.Record;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("kafka")
public class UserResource {

//    List<emp> empList = new ArrayList<>();
    List<User> userList = new ArrayList<>();

//    @Autowired
//    KafkaTemplate<String, emp> kafkaTemplate;
    @Autowired
    KafkaTemplate<String, User> userKafkaTemplate;
//    public static final String TOPIC = "quickstart-event";
    public static final String TOPIC = "kafkaUser";

    @GetMapping("/consume")
    public List<User> post(){
        return userList;
    }
//    @PostMapping(value = "/save")
//    public String save(@RequestBody emp e){
//        kafkaTemplate.send(TOPIC,TOPIC,e);
//        return e.toString();
//    }

    @PostMapping(value = "/users")
    public User save(@RequestBody User user){
        userKafkaTemplate.send(TOPIC,TOPIC,user);
        return user;
    }


//    @KafkaListener(topics = "kafkaExp", groupId = "kafkaListenersGroup", containerFactory = "containerFactory")
//    public void getMessages(GenericMessage message){
////        empList.add(e);
//        GenericRecord record = (GenericRecord) message.getPayload();
//
//        empList.add(emp.newBuilder()
//                .setName(record.get("name").toString())
//                .setAge((Integer) record.get("age"))
//                .setId((Integer) record.get("id"))
//                .setSalary((Integer) record.get("salary"))
//                .setAddress(record.get("address").toString())
//                .build());
//    }
    @KafkaListener(topics = TOPIC, groupId = "kafkaUserListenersGroup", containerFactory = "userContainerFactory")
    public void getUserMessages(ConsumerRecord<String, User> record){
        userList.add(record.value());
    }
}

