package com.example;

import com.fasterxml.jackson.databind.JsonSerializer;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.concurrent.Future;

public class KafkaProducerV1 {
    public static void main(String[] args) throws UnknownHostException {

        String topic = "customer-topic";

        Properties config = new Properties();
        config.put("bootstrap.servers", "127.0.0.1:9092");
        config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);
        config.put("schema.registry.url","http://127.0.0.1:8081");

//        Properties properties = new Properties();
//        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
//        properties.setProperty(ProducerConfig.ACKS_CONFIG, "1");
//        properties.setProperty(ProducerConfig.RETRIES_CONFIG, "10");
//        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
//        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class.getName());
//        properties.setProperty("schema.registry.url", "http://127.0.0.1:8081");

        KafkaProducer<String, Customer> kafkaProducer = new KafkaProducer<String, Customer>(config);

        Customer customer = new Customer("john","cena",34);
        final ProducerRecord<String, Customer> record = new ProducerRecord<>(topic, topic, customer);
        Future<RecordMetadata> future = kafkaProducer.send(record);
//
//        ProducerRecord<String, Customer> producerRecord = new ProducerRecord<String, Customer>(topic, customer);
//        kafkaProducer.send(producerRecord, (recordMetadata, e) -> {
//            if (e == null) {
//                System.out.println("Success!");
//                System.out.println("Metadata: " + recordMetadata.toString());
//            } else {
//                e.printStackTrace();
//            }
//        });
//        kafkaProducer.flush();
//        kafkaProducer.close();
    }
}
