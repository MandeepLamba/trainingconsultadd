package com.mandeep.springjparepository.model;

public class Result {

    private Float sub1,sub2,sub3,sub4;

    public Float getSub1() {
        return sub1;
    }

    public void setSub1(Float sub1) {
        this.sub1 = sub1;
    }

    public Float getSub2() {
        return sub2;
    }

    public void setSub2(Float sub2) {
        this.sub2 = sub2;
    }

    public Float getSub3() {
        return sub3;
    }

    public void setSub3(Float sub3) {
        this.sub3 = sub3;
    }

    public Float getSub4() {
        return sub4;
    }

    public void setSub4(Float sub4) {
        this.sub4 = sub4;
    }
}
