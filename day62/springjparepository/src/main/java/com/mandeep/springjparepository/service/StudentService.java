package com.mandeep.springjparepository.service;

import com.mandeep.springjparepository.model.Student;
import com.mandeep.springjparepository.repository.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService {

    @Autowired
    private StudentRepo studentRepo;

    public Student saveStudent(Student student){
        return studentRepo.save(student);
    }
}
