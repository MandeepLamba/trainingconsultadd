package com.mandeep.springjparepository.repository;

import com.mandeep.springjparepository.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;


public interface StudentRepo extends JpaRepository<Student, Integer> {

}
