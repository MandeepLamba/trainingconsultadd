package com.mandeep.springjparepository;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringjparepositoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringjparepositoryApplication.class, args);
	}

}
