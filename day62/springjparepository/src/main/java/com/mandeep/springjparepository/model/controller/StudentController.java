package com.mandeep.springjparepository.model.controller;

import com.mandeep.springjparepository.model.Student;
import com.mandeep.springjparepository.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {

    @Autowired
    private StudentService service;

    @PostMapping("/save")
    public Student save(@RequestBody Student student){
        return service.saveStudent(student);
    }
}
