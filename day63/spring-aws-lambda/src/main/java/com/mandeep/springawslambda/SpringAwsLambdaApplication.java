package com.mandeep.springawslambda;

import com.mandeep.springawslambda.model.User;
import com.mandeep.springawslambda.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringAwsLambdaApplication {
	@Autowired
	private UserRepo userRepo;

	public static void main(String[] args) {
		SpringApplication.run(SpringAwsLambdaApplication.class, args);
	}

	@Bean
	public User getUser(Integer id){
		return userRepo.getUser(id);
	}
}
