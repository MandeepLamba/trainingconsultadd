package com.mandeep.springawslambda.repo;

import com.mandeep.springawslambda.model.Order;
import com.mandeep.springawslambda.model.User;
import org.springframework.stereotype.Repository;

import java.util.*;


@Repository
public class UserRepo{

    private Map<Integer, User> users;

    public UserRepo() {
        users = new HashMap<>();
        users.put(1,new User(1,"Aman",
                Arrays.asList(new Order(101,"Coffee", new Date(2021,Calendar.FEBRUARY,12)),
                        new Order(102,"Tea", new Date(2021, Calendar.FEBRUARY,13)),
                        new Order(103,"Latte", new Date(2021,Calendar.MARCH,16)))));

        users.put(2,new User(2,"Sonu",
                Arrays.asList(new Order(104,"Brew", new Date(2021,Calendar.DECEMBER,12)),
                        new Order(105,"Cold Drink", new Date(2021, Calendar.JULY,13)),
                        new Order(106,"Roll", new Date(2021,Calendar.JUNE,16)))));

        users.put(3,new User(3,"Kamal",
                Arrays.asList(new Order(107,"Rice", new Date(2021,Calendar.MAY,12)),
                        new Order(108,"Shake", new Date(2021, Calendar.FEBRUARY,13)),
                        new Order(109,"Bear", new Date(2021,Calendar.OCTOBER,16)))));

    }

    public User getUser(int userID){
        return users.get(userID);
    }

    public List<Order> getOrderList(int userId){
        return users.get(userId).getOrderList();
    }
}
