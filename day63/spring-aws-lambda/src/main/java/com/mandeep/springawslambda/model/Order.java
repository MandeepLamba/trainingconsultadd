package com.mandeep.springawslambda.model;

import java.util.Date;

public class Order {
    private int id;
    private String item;
    private Date date;

    public Order(int id, String item, Date date) {
        this.id = id;
        this.item = item;
        this.date = date;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", item='" + item + '\'' +
                ", date=" + date +
                '}';
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
