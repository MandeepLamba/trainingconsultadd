package com.mandeep.topic;

public record Topic(String id, String name,String description) {
}
