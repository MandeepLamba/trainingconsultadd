// let user = {
//     name: "man",
//     age : 23
// }
// let user1 = {
//     name: "man2",
//     age : 24
// }

// let clone = {}
// Object.assign(clone,user,user1);
// console.log("User: ", user, " User: ", user1);
// console.log("Clone: ", clone);

/*
1. Write a JS code to find whether two strings are anagram of each other.​
 An anagram of a string is another string that contains the same characters, 
only the order of characters can be different. 
For example, “abcd” and “dabc” are anagram of each other. 
Similarly “listen” and “Silent”. Note- do not consider the case of letters in words.
*/

// let s1 = "listen";
// let s2 = "Silent";
// console.log("1.",[...s1.toLowerCase()].sort().join("") === [...s2.toLowerCase()].sort().join(""));

/*
2. Write a function in JS that takes a string consisting of only round brackets as an input
and checks if the bracket sequence is regular or not. ​A bracket sequence is called regular
if it is possible to insert some numbers and signs into the sequence in such a way that the
new sequence will represent a correct arithmetic expression.
Example: “((()))” should return true as it is a regular bracket sequence but “()(()” and “)(“ should return false.
*/

// console.log(check(")(()"));
// function check(s) {
//     let count = 0;
//     for (let i = 0; i < s.length; i++) {
//         let ss = s[i];
//         if (ss == "(") count++;
//         else {
//             if (!count) return false;
//             count--
//         }
//     }
//     return !count
// }


/*
3.Given a boolean 2D array where each row is sorted.
Find the row with the maximum number of ​1s​. Your input must be :
- First line containing m and n where m is no of rows and n is no of columns.
- Second line containing the array elements. Example - Input
44
0 1 1 1 0 0 1 1 1 1 1 10 0 0 0
Output - 2 , as row 2 has max no of 1’s (zero based indexing)
*/

// let m = 4, n = 4;
// let input = "0 1 1 1 0 0 1 1 1 1 1 1 0 0 0 0"

// let arr1 = input.split(" ").map(Number);
// let totals = []

// while (arr1.length) {
//     totals.push(arr1.splice(0, 4).reduce((total, num) => total + num));
// }

// console.log(totals.indexOf(Math.max(...totals)));

/*
4.Given two strings ​str1​ and ​str2​ and below operations that can be performed on str1.
Find the minimum number of edits (operations) required to convert ‘str1′ into ‘str2′.
- Insert, remove , replace All of the above operations are of cost=1. Both the strings are of lowercase.
Example - str1 = “peek” str2 = “peeks” then output = 1 as one operation is required to make 2 strings the same i.e. removing 's' from str2.

let str1 = "peek"
let str2 = "peeks"


/*
5.You are an agent and you need to find the total number of ways a message can be decoded.
Any message is encoded as follows:
A -1, B - 2, C - 3......Z - 26.
An empty digit string will have only single decoding. It may be assumed that the input contains
valid digits from 0 to 9 and If there are leading 0’s, extra trailing 0’s and two or more
consecutive 0’s then it is an invalid string.
Example - 123 as input can be decoded as “ABC” (1 2 3), “LC” (12 3) or “AW” (1 23). So total ways are 3 which will be the output.
*/

// let count = 0;
// console.log(decode(123407));

// function decode(input) {
//     String(input).split("").forEach((element)=>{
//         if (Number(element)) count++
//     })
//     input = String(input);
//     for (let i = 0; i < input.length-1; i++) {
//         let num = Number(String(input).split("").splice(i,2).join(""));
//         if (num>9 && num<27) {
//             count++;
//         }

//     }
//     return("Total: " + count)
// }

/*
6.Given a string S. Write a code to print all possible permutations of the string.
Also find the first non repeating character in this string. Example - S = “hiokjholiw” output = k
*/

// let s = "hiokjholiw";
// // let s = "ABWE"
// let c = 1;
// permu(s, 0,s.length-1)

// function permu(s, l,r) {
//     if (l==r) {
//         console.log(s);
//     } else {
//         for (let i = l; i <= r; i++) {
//             s = swap(s,l,i)
//             permu(s,l+1,r)
//             s = swap(s,l,i)
//         }
//     }
// }

// function swap(s,i,j) {
//     let l = String(s).split("");
//     [l[i],l[j]] = [l[j],l[i]]
//     return l.join("")
// }
