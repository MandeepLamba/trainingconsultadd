package com.example.demospring.controller;

import com.example.demospring.services.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class Controller {

    @Autowired
    Service service;

    @RequestMapping(method = RequestMethod.POST, value = "/hello")
    public String welcome(@RequestBody User1 user1){
        System.out.println(service.toString());
        service.Print();
        return "<h1>Welcome User: "+ user1.name() + " ID: "+ user1.id() + "</h1>";
    }


    private record User1(String name, int id){}
}
