package com.example.demospring;

import com.example.demospring.services.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemospringApplication {



	public static void main(String[] args) {
		SpringApplication.run(DemospringApplication.class, args);
	}



}
