package com.example.demospring.controller;

import com.example.demospring.services.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller2 {

    @Autowired
    Service service;

    @RequestMapping(method = RequestMethod.POST, value = "/hello1")
    public String welcome(@RequestBody User1 user1){
        System.out.println(service.toString());
        service.Print();
        return "<h1>Welcome User: "+ user1.name() + " ID: "+ user1.id() + "</h1>";
    }


    private record User1(String name, int id){}
}
