package com.example.mysqlspringboot.con;

import com.example.mysqlspringboot.model.User;
import com.example.mysqlspringboot.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class Controller {

    @Autowired
    UserRepo userRepo;

    @PostMapping("/users/add")
    public String addUser(@RequestBody User user){
        userRepo.save(user);
        return "Saved";
    }
    @PostMapping("/users")
    public User findUser(@RequestParam Integer id){
        return userRepo.findUserById(id);
    }

    @GetMapping("/users")
    public List<User> getUsers(){
        return (List<User>) userRepo.findAll();
    }

}
