package com.example.mysqlspringboot.repo;


import com.example.mysqlspringboot.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepo extends CrudRepository<User, Integer> {
    public User findUserById(Integer id);

}
