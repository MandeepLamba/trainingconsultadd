package com.mandeep;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Account account = new Account();
        new Thread(() -> {
                for(;;){
                    account.withdraw(5);
                    try {
                        Thread.sleep(1000);
                    } catch (Exception e){}
                }
        }).start();
        new Thread(() -> {
            for(;;) {
                account.deposit(15);
                try {
                    Thread.sleep(5000);
                } catch (Exception e){ }
            }

        }).start();
    }
}

class Account{
    private String name;
    private int balance=0;
    public int getBalance(){
        return balance;
    }

    public synchronized void withdraw(int amount){

        if(balance<amount){
            try {
                wait();
            } catch (InterruptedException e) {
                System.out.println("Exception : " + e.getMessage());
            }
        }
        else {
            balance-=amount;
            System.out.println("Withdraw: " + balance);
        }
    }

    public synchronized void deposit(int amount){
        if(amount>0){
            balance+=amount;
            System.out.println("Deposited " +  balance);
            notify();
        }
    }
}
