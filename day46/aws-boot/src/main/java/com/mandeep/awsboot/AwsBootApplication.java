package com.mandeep.awsboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class AwsBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(AwsBootApplication.class, args);
	}

	@GetMapping("/")
	public String welcome(){
		return "<center><h1>Connected</h1><center>";
	}
}
