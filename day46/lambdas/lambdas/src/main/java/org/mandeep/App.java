package org.mandeep;


import com.amazonaws.services.lambda.runtime.Context;

public class App
{
   public String handleRequest(String input, Context context){
       context.getLogger().log("\n\n----Input: "+ input + "\n\n----");
       return "Welcome " + input + " !";
   }
}
