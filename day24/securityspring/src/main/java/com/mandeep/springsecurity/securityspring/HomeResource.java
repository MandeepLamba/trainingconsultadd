package com.mandeep.springsecurity.securityspring;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeResource {

    @RequestMapping
    public String home(){
        return "<h1>Welcome</h2>";
    }
}
