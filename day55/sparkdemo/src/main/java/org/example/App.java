package org.example;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        SparkConf sparkConf = new SparkConf().setAppName("springdemo");

        //JavaSparkContext context = new JavaSparkContext(sparkConf);

        //JavaRDD<String> textFile = context.textFile("");
        System.out.println("Completed!");
    }
}
