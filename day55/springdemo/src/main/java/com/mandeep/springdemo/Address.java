package com.mandeep.springdemo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class Address {

    private String city;
    private int zip;
    private String state;

    

}
