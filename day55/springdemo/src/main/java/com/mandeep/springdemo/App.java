package com.mandeep.springdemo;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

/**
 * Hello world!
 *
 */
public class App 
{

    public static void main( String[] args )
    {
//        Resource resource = new ClassPathResource("applicationContext.xml");
//        BeanFactory factory = new XmlBeanFactory(resource);

        ApplicationContext  context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Student student = context.getBean(Student.class);
        System.out.println(student);

        System.out.printf("%8s%15s%15s%25s%25s","ID","First Name", "Last Name", "Marks", "Address");
        for (int i = 0; i < 5; i++) {
            System.out.printf("\n%8s%15s%15s%25s%50s",
                    student.getId(),
                    student.getFname(),
                    student.getLname(),
                    student.getMarks(),
                    student.getAddress());
        }



    }

}

