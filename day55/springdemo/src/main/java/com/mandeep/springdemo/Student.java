package com.mandeep.springdemo;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class Student {

	private int id;
	private String fname;
	private String lname;
	private List<Integer> marks;
	private Address address;


}
