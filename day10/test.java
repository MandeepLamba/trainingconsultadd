import java.math.BigInteger;
import java.util.*;
public class test{
	
	public static void main(String []argh)
	{
		Scanner sc = new Scanner(System.in);
		while (sc.hasNext()) {
			String input=sc.next();
		    Stack<String> s = new Stack<>();  
            HashMap<String,String> sy = new HashMap<>();
            sy.put("}","{");
            sy.put("]","[");
            sy.put(")","(");         
            //Complete the code
            boolean check = true;
            for(int i=0; i<input.length();i++){
                String ch = Character.toString(input.charAt(i));
                if(sy.containsValue(ch)){
                    s.push(ch);
                } else{
                    if(s.empty()){
                        check = false;
                        break;
                    }   
                    else{
                        String p = s.pop().toString();
                        if(!p.equals(sy.get(ch))){
                            check=false;
                        }
                    }   
                }
            }
            
            if(s.empty()&&check) System.out.println(true);
            else System.out.println(false);
		}
		sc.close();
	}
}


class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        BigInteger a = sc.nextBigInteger();
        BigInteger b = sc.nextBigInteger();
        System.out.println(a.add(b));
        System.out.println(a.multiply(b));
        sc.close();
    }
}