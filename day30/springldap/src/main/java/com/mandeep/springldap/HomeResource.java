package com.mandeep.springldap;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeResource {
    @RequestMapping("/")
    public String home(){
        return "<h1>Welcome To Website</h2>";
    }
}
