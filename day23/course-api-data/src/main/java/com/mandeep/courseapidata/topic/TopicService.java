package com.mandeep.courseapidata.topic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TopicService {

    @Autowired
    private TopicRepo topicRepo;

    public List<Topic> getAllTopics(){
        System.out.println("TopicService.getAllTopics");
        List<Topic> topics = new ArrayList<>();
        topicRepo.findAll().forEach(topics::add);
        return topics;
    }
    public Topic getTopic(String id){
        Topic topic = new Topic();
        Optional<Topic> result = topicRepo.findById(id);
        if(result.isPresent()) topic = result.get();
        return topic;
    }
    public void addTopic(Topic topic){
        topicRepo.save(topic);
    }

    public void updateTopic(Topic topic) {
        topicRepo.save(topic);
    }

    public void deleteTopic(String id) {
        topicRepo.deleteById(id);
    }
}
