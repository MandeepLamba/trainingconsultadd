package com.mandeep.courseapidata.topic;

import org.springframework.data.repository.CrudRepository;

public interface TopicRepo extends CrudRepository<Topic, String> {

}
