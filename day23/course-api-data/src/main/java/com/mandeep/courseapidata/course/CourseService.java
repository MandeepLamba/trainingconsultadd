package com.mandeep.courseapidata.course;

import com.mandeep.courseapidata.topic.Topic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CourseService {

    @Autowired
    private CourseRepo courseRepo;

    public List<Course> getAllCourses(String topicId){
        return new ArrayList<>(courseRepo.findByTopicId(topicId));
    }
    public Course getCourse(String id){
        return courseRepo.findById(id).get();
    }
    public void addCourse(String topicId, Course course){
        course.setTopic(new Topic(topicId,"",""));
        courseRepo.save(course);
    }

    public void updateCourse(Course course) {
        courseRepo.save(course);
    }

    public void deleteCourse(String id) {
        courseRepo.deleteById(id);
    }
}
