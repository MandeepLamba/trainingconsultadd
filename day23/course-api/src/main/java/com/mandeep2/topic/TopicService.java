package com.mandeep2.topic;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TopicService {
    private List<Topic> topics = new ArrayList<>(Arrays.asList(
            new Topic("spring","Spring Framework", "Spring Framework Discription"),
            new Topic("java","Java Framework", "Java Discription"),
            new Topic("javascript","JavaScript Framework", "JavaScript Discription")));
    public List<Topic> getAllTopics(){
        return topics;
    }
    public Topic getTopic(String id){
        return topics.stream().filter(topic -> topic.id().equals(id)).findFirst().get();
    }
    public void addTopic(Topic topic){
        System.out.println("Adding Topic : " + topic);
        topics.add(topic);
    }

    public void updateTopic(Topic topic, String id) {
        topics = topics.stream().map(topic1 -> {
            if(topic1.id().equals(id)){
                return topic;
            }
            return topic1;
        }).collect(Collectors.toList());
    }

    public boolean deleteTopic(String id) {
        return topics.removeIf(topic -> topic.id().equals(id));
    }
}
