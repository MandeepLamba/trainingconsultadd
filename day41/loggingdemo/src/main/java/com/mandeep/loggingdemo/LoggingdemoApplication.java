package com.mandeep.loggingdemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@SpringBootApplication
public class LoggingdemoApplication {

    static Logger logger = LoggerFactory.getLogger(LoggingdemoApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(LoggingdemoApplication.class, args);

        UserStates userStates = new UserStates();
        Optional<Long> optionalLong = Optional.of(4L);
        userStates.setVisitCount(optionalLong);

        UserStates userStates1 = new UserStates();
        Optional<Long> optionalLong1 = Optional.empty();
        userStates1.setVisitCount(optionalLong1);

        UserStates userStates2 = new UserStates();
        Optional<Long> optionalLong2 = Optional.of(5L);
        userStates2.setVisitCount(optionalLong2);


        Map<String, UserStates> e1 = new HashMap<>();
        e1.put("3455", userStates);
        e1.put("sdkfj", userStates1);
        e1.put("2344242", userStates2);

        Map<String, UserStates> e2 = new HashMap<>();
        e2.put("3455", userStates2);
        e2.put("sdkfj", userStates);
        e2.put("2344242", null);

        System.out.println(count(e1, e2));

    }

    public static Map<Long, Long> count(Map<String, UserStates>... visits) {
        if (visits.length < 1) return new HashMap<>();
        else {
            Map<Long, Long> res = new HashMap<>();
            Arrays.stream(visits)
                    .forEach(visit -> {
                        visit.forEach((k, v) -> {
                            if (v != null) {
                                try {
                                    if (v.getVisitCount().isPresent()) {
                                        Long key = Long.parseLong(k);
                                        if (res.containsKey(key)) {
                                            res.put(key, res.get(key) + v.getVisitCount().get());
                                        } else {
                                            res.put(key, v.getVisitCount().get());
                                        }
                                    }
                                } catch (Exception e){ }
                            }
                        });
                    });
            return res;
        }
    }

    private static class UserStates {

        private Optional<Long> visitCount = Optional.empty();

        public Optional<Long> getVisitCount() {
            return visitCount;
        }

        public void setVisitCount(Optional<Long> visitCount) {
            this.visitCount = visitCount;
        }

        @Override
        public String toString() {
            return "UserStates{" +
                    "visitCount=" + visitCount +
                    '}';
        }
    }

}
