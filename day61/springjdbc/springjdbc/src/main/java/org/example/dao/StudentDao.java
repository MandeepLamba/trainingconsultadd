package org.example.dao;

import org.example.model.Student;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StudentDao {
    private JdbcTemplate jdbcTemplate;


    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Student> getStudents(){

        String q = "select * from student";

        return jdbcTemplate.query(q, new ResultSetExtractor<List<Student>>() {
            @Override
            public List<Student> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                List<Student> list = new ArrayList<Student>();
                while (resultSet.next()) {
                    Student e = new Student();
                    e.setRoll(resultSet.getInt(2));
                    e.setName(resultSet.getString(1));
                    list.add(e);
                }
                return list;
            }
        });
    }
}
