package org.example;

import org.example.dao.StudentDao;
import org.example.model.Student;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        StudentDao dao = context.getBean(StudentDao.class);
        Student student = context.getBean(Student.class);

//        System.out.println(dao.saveStudent(student));

        System.out.println(dao.getStudents());
    }
}
