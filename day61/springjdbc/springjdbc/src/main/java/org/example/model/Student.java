package org.example.model;

import lombok.Data;

@Data
public class Student{
    private int roll;
    private String name;
}
