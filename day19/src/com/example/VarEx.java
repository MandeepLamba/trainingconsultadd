package com.example;

import java.time.LocalDate;
import java.util.Arrays;

public class VarEx {
    public static void main(String[] args) {
        var name = "Mandeep";
        var date = LocalDate.now();
        var count = 45;
        var stream = Arrays.asList("Hello", "World");

        System.out.println("stream.forEach(); = " + stream);
        System.out.println("count = " + count);
        System.out.println("date = " + date);
        System.out.println("name = " + name);

        //with lambda
        var teachers = Teachers.getAll();
        teachers.forEach(teacher -> VarEx.print(teacher.getName()));


    }
    private static void print(String s){
        System.out.println("s = " + s);
    }
}
