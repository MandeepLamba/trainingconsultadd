package com.example;

public class SwitchExpressionEx {

    public static void main(String[] args) {
        int i = 9;
        String result = switch (i) {
            case 1, 3, 5, 7, 9 -> "Odd Number";
            case 2, 4, 6, 8 -> "Even Number";
            default -> "Not in range";
        };
        System.out.println("result = "+ i +" is " + result);

        //action in cases
        switch (i){
            case 1, 3, 5, 7, 9 -> System.out.println("result = "+ i +" is odd number");
            case 2, 4, 6, 8 -> System.out.println("result = "+ i +" is even number");
            default -> System.out.println("result = "+ i +" is out of Range");
        }

        //with yield
        result = switch (i) {
            case 1, 3, 5, 7, 9 -> {
                yield "Odd Number";
            }
            case 2, 4, 6, 8 -> "Even Number";
            default -> "Not in range";
        };
        System.out.println("result = "+ i +" is " + result);

    }
}
