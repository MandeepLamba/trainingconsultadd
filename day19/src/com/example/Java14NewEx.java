package com.example;

public class Java14NewEx {
    public record Student(String name,String lname){};
    public static void main(String[] args) {
//        String name = null;
//        System.out.println("name.toUpperCase() = " + name.toUpperCase());

        Student student = new Student("Mandeep","Lamba");
        System.out.println(student.name());
        System.out.println(student.lname());
        System.out.println(student.name);
        System.out.println(student.lname);

    }
}
