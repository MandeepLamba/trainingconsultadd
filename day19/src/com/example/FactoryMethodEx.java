package com.example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class FactoryMethodEx {
    public static void main(String[] args) {

        //pre Java9
        List<String> names = new ArrayList<>();
        names.add("Mike");
        names.add("Sumu");
        names.add("Soni");
        names = Collections.unmodifiableList(names);
        System.out.println("names = " + names);

        //factory methods in java 9
        List<String>  names1 = List.of("Mike","Sonu","Sam");
        System.out.println("names1 = " + names1);

//        Set<String> strings = Set.of("sam","sam","user");
//        names.add("add");


        
    }
}
