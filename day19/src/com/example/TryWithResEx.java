package com.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

public class TryWithResEx {
    public static void main(String[] args) {
        Reader reader = new StringReader("Hello World \nWelcome");
        BufferedReader bufferedReader = new BufferedReader(reader);
        try(bufferedReader){
            bufferedReader.lines().forEach(System.out::println);
        } catch (IOException e){
            System.out.println("--IOException--");
        }
    }
}
