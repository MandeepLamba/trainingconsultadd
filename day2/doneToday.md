## Installations:
- git
- brew
- MySQL Ver 8.0.24
- PostgreSQL-13 (pgadmin4)
- python3
- OpenJDK 16.0.1
- NodeJS 14.16.1 LTS
- NPM 6.14.12


## Videos Watched:

- APIs: REST, SOAP
    - https://www.youtube.com/watch?v=rtWH70_MMHM
    - https://www.youtube.com/watch?v=7YcW25PHnAA
    - https://www.youtube.com/watch?v=TvGLm7BijJY
    - https://www.youtube.com/watch?v=tI8ijLpZaHk
- Frontend, Backend
    - https://www.youtube.com/watch?v=lAT9kahUMH4
    - ...
        