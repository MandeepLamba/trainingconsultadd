#!/bin/bash
set -eo pipefail
gradle -q packageLibs
mv build/distributions/CreateLambda.zip build/CreateLambda-lib.zip